#! /usr/bin/env python3

import argparse
import os
import shlex
import subprocess
import sys

verbose=True

default_root = 'pf.engine'
host = 'linux' if sys.platform == 'linux2' else sys.platform

# Target platforms supported by each host
host_targets={
    'win32' : ['android', 'win10', 'win32', 'emscripten'],
    'darwin': ['android', 'ios', 'mac', 'emscripten'],
    'linux' : ['android', 'linux', 'emscripten'],
}
# Default target platform for each host
default_target={
    'win32' : 'win32',
    'darwin': 'mac',
    'linux' : 'linux',
}

generator_types={
    'msvs',
    'xcode',
    'makefile',
    'ninja',
}

target_generator_types={
    'win32'     : ['msvs', 'makefile', 'ninja'],
    'win10'     : ['msvs', 'makefile', 'ninja'],
    'mac'       : ['xcode', 'makefile', 'ninja'],
    'ios'       : ['xcode', 'makefile', 'ninja'],
    'android'   : ['ninja', 'makefile'],
    'linux'     : ['ninja', 'makefile'],
    'emscripten': ['ninja', 'makefile'],
}

target_archs={
    'win32'     : ['x64', 'x86', 'arm', 'arm64'],
    'win10'     : ['x64', 'x86', 'arm', 'arm64'],
    'mac'       : [None],
    'ios'       : [None],
    'android'   : ['arm64-v8a', 'armeabi-v7a'],
    'linux'     : ['x86_64', 'arm64'],
    'emscripten': [None],
}

# Which cmake generator name corresponds to generator type
cmake_generators = {
    'msvs'    : ['Visual Studio 15 2017',
                 'Visual Studio 16 2019',
                 'Visual Studio 17 2022'],
    'xcode'   : 'Xcode',
    'makefile': 'NMake Makefiles' if host == 'win32' else 'Unix Makefiles',
    'ninja'   : 'Ninja Multi-Config',
}


def get_cmake_executable(args):
    cmake_executable='cmake'
    if not args.system_cmake:
        if host == 'win32':
            cmake_executable='tools/cmake/cmake-win64/bin/cmake.exe'
        elif host == 'linux':
            cmake_executable='tools/cmake/cmake-linux/bin/cmake'
        elif host == 'darwin':
            cmake_executable='tools/cmake/cmake-macos/bin/cmake'
            if not os.path.exists(os.path.join(args.root, cmake_executable)):
                cmake_executable='tools/cmake/cmake-macos/CMake.app/Contents/bin/cmake'
        cmake_executable=os.path.abspath(os.path.join(args.root, cmake_executable)).replace('\\', '/')
    return cmake_executable


def get_linux_arm64_cmake_toolchain_file(args):
    return os.path.abspath(os.path.join(args.root, 'build/cmake/toolchains/linux-arm64-cross.cmake'))


def make_cmake_generator_option(args):
    cmake_g_opt=''
    cmake_a_opt=''
    cmake_t_opt=''
    if args.generator == 'msvs':
        index = 2
        if args.vs == '2017':
            index = 0
        elif args.vs == '2019':
            index = 1
        elif args.vs == '2022':
            index = 2
        cmake_g_opt = cmake_generators['msvs'][index]
        cmake_a_opt=args.arch
    else:
        cmake_g_opt=cmake_generators[args.generator]

    r = '-G"{}"'.format(cmake_g_opt)
    if cmake_a_opt:
        r += ' -A {}'.format(cmake_a_opt)
    if cmake_t_opt:
        r += ' -T {}'.format(cmake_t_opt)

    if args.generator in ['makefile']:
        r += ' -DCMAKE_BUILD_TYPE={}'.format(args.build_type)
    return r


def parse_cmdline():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target",
                        choices=host_targets[host],
                        default=default_target[host],
                        help="Generate project for target platform, if omitted target platform is the host platform")

    parser.add_argument("--root",
                        default=default_root,
                        help="Engine root directory")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("-g", "--generator",
                        choices=generator_types,
                        dest='generator',
                        help="Select project generator type")
    group.add_argument("--ninja",
                        action='store_const',
                        const='ninja',
                        dest='generator',
                        help="Select ninja generator")
    group.add_argument("--makefile",
                        action='store_const',
                        const='makefile',
                        dest='generator',
                        help="Select makefile generator")

    parser.add_argument("--arch",
                        help="Generate project for given architecture")

    group = parser.add_mutually_exclusive_group()
    group.add_argument("--build-type",
                        default=None,
                        dest='build_type',
                        help="Set build type for makefile and ninja generator. Default is 'Debug'")
    group.add_argument("--release",
                       default=None,
                       action='store_const',
                       const='Release',
                       dest='build_type',
                       help="Set 'Release' build type for makefile generator")

    parser.add_argument("--fresh",
                        help="Perform a fresh configuration (--fresh option for CMake 3.24+)",
                        action="store_true")
    parser.add_argument("--system-cmake",
                        help="Use cmake installed in system and accessible through $PATH environment variable",
                        action="store_true")
    parser.add_argument("--vs",
                        choices=['2017', '2019', '2022'],
                        default='2019',
                        help="Select Visual Studio version")
    parser.add_argument("-r", "--crash-reporting",
                        help="Enable crash reporting for application (-DPF_ENABLE_CRASHREPORTING=1)",
                        action="store_true")
    parser.add_argument("--no-asserts",
                        help="Do not enable asserts in all build configurations (do not pass -DPF_ASSERT_ALWAYS=1)",
                        action="store_true")
    parser.add_argument("--test-coverage",
                        help="Enable test coverage (-DPF_ENABLE_COVERAGE=1)",
                        action="store_true")
    parser.add_argument("--default-python-venv",
                        help="Default python venv (do not pass -DPF_CUSTOM_PYTHON_VENV_PATH=<..>)",
                        action="store_true")
    parser.add_argument("--skip-python-setup",
                        help="Skip python setup (-DPF_SKIP_PYTHON_SETUP=1)",
                        action="store_true")
    parser.add_argument("--unity",
                        help="Unity build (-DPF_ENABLE_UNITYBUILD=1)",
                        action="store_true")

    parser.add_argument("-D",
                        action="append",
                        help="Additional arguments passed to cmake through -D option")

    #parser.add_argument("--winsdk",
    #                    help="Windows SDk version, e.g. 10.0.10240.0")
    parser.add_argument("-n", "--just-print",
                        help="Just print commands, do not actually run them",
                        action="store_true")
    parser.add_argument("-o", "--outdir",
                        help="Generated project will be placed in given dir. Default value is './build/target/project_name'")
    parser.add_argument("--preset",
                        default=None,
                        help="Pass preset to cmake with option 'cmake --preset X'")
    parser.add_argument("--build",
                        action="store_true",
                        help="Build generated project using command 'cmake --build'")
    parser.add_argument("--open",
                        help="Open generated project if it is possible",
                        action="store_true")
    parser.add_argument("project")
    return parser.parse_args()


####################################################################################

def print_if_v(s):
    if verbose or s.startswith('error:'):
        print (s)
        
def error(s):
    #print('/033[93merror: ' + s + '/033[0m')
    print('error: ' + s)
    
def warning(s):
    #print('/033[93mwarning: ' + s + '/033[0m')
    print('warning: ' + s)
    
def info(s):
    #print('\033[33;20minfo: ' + s + '\033[0m')
    print('info: ' + s)

####################################################################################

def find_project(args):
    project = os.path.normpath(args.project).replace('\\', '/')

    search_dirs = []
    project_root = args.root
    project_parts = project.split('/')
    if project in ['all']:
        search_dirs.append(project_root)
    elif os.path.isdir(project_parts[0]):
        project_root = project_parts[0]
        search_dirs.append(project)
    else:
        search_dirs.append(os.path.join(project_root, *project_parts).replace('\\', '/'))
        search_dirs.append(os.path.join(project_root, 'programs', *project_parts).replace('\\', '/'))
        search_dirs.append(os.path.join(project_root, 'modules', *project_parts).replace('\\', '/'))
        if len(project_parts) > 1:
            search_dirs.append(os.path.join(project_root, 'games', project_parts[0], 'programs', *project_parts[1:]).replace('\\', '/'))
        search_dirs.append(os.path.join(project_root, 'games', *project_parts).replace('\\', '/'))

    project_dir = None
    for w in search_dirs:
        info("checking project in '{}' ..".format(w))
        cmakelists_file=os.path.join(w, "CMakeLists.txt")
        if os.path.isdir(w) and os.path.exists(cmakelists_file):
            project_dir = w
            break

    build_dir = ''
    if project_dir == project_root:
        build_dir = 'all'
    elif project_dir is not None:
        exclude = [
            project_root,
            'games',
            'modules',
            'programs',
        ]
        project_parts = project_dir.split('/')
        for part in project_parts:
            if part not in exclude:
                build_dir = os.path.join(build_dir, part)
    return (project_dir, project_root, build_dir)

####################################################################################

def run_vcvars(target, arch, winsdk):
    arch_map={
        'x86'  : 'amd64_x86',
        'x64'  : 'amd64',
        'arm'  : 'amd64_arm',
        'arm64': 'amd64_arm64'
    }
    vcvarsall='c:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvarsall.bat'
    if not os.path.exists(vcvarsall):
        vcvarsall='c:/Program Files (x86)/Microsoft Visual Studio/2017/Community/VC/Auxiliary/Build/vcvarsall.bat'
    cmd='{} {} {} {}'.format(
        vcvarsall,
        arch_map[arch],
        'uwp' if target == 'win10' else '',
        winsdk if winsdk != None else '')
    cmd += ' && set'

    info("running vcvarsall: {}".format(cmd))
    out,_=subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()
    env_vars=out.decode('latin-1').split('\r\n')
    
    env=os.environ
    for line in env_vars:
        parts=line.strip().split('=')
        if len(parts) == 2:
            key=parts[0].strip()
            value=parts[1].strip()
            env[key]=value
    return env

def prepare_environment(args):
    env = None
    if host == 'win32':
        if args.generator in ['makefile', 'ninja']:
            if args.target == 'android':
                env = run_vcvars('win32', 'x64', None)
            else:
                env = run_vcvars(args.target, args.arch, None)
    return env

####################################################################################

def main():
    args = parse_cmdline()

    args.root = os.path.normpath(args.root)
    if args.target == None:
        args.target = default_target[host]
    if args.target not in host_targets[host]:
        error('target `{}` is not supported by `{}`'.format(args.target, host))
        sys.exit(1)

    if args.generator == None:
        args.generator = target_generator_types[args.target][0]
    if args.generator not in target_generator_types[args.target]:
        error('generator `{}` is not supported by target `{}`'.format(args.generator, args.target))
        sys.exit(1)

    if args.arch == None:
        args.arch = target_archs[args.target][0]
    if args.arch not in target_archs[args.target]:
        error('arch `{}` is not applied to target `{}`'.format(args.arch, args.target))
        sys.exit(1)

    if args.build_type != None and args.generator not in ['makefile', 'ninja']:
        warning('--build-type option is applied only to `makefile` and `ninja` generators. Ignored')
    if args.build_type == None and args.generator in ['makefile', 'ninja']:
        args.build_type = 'Debug'

    if args.generator in ['makefile', 'ninja'] and args.target in ['win32', 'win10']:
        pass  # run vcenv script and get environment variables
    if host == 'win32' and args.target == 'android':
        pass  # run vcenv script and get environment variables

    ##################
    project_dir, project_root, build_dir_tail=find_project(args)
    if project_dir is None:
        error("project '{}' is not found".format(args.project))
        sys.exit(1)
    info("found project in '{}'".format(project_dir))
    
    ##################
    venv_dir=os.path.abspath(os.path.join("build", project_root, "venv", 'default' if host != 'linux' else 'linux')).replace('\\', '/')

    build_dir = os.path.join('build', project_root, args.target)
    if args.arch is not None and args.target in ['android', 'linux']:
        build_dir = os.path.join(build_dir, args.arch)
    if args.outdir is not None:
        build_dir = os.path.join(build_dir, args.outdir)
    else:
        build_dir = os.path.join(build_dir, build_dir_tail)
    build_dir=os.path.abspath(build_dir).replace('\\', '/')

    if not args.just_print:
        info("making directories: {}".format(build_dir))
        if not os.path.exists(build_dir):
            os.makedirs(build_dir)

    ##################
    cmake_extra_variables = ''
    if project_root != args.root:
        cmake_extra_variables += ' -DPF_ENGINE_PATH={}'.format(os.path.abspath(args.root).replace('\\', '/'))
    if args.target == 'android':
        cmake_extra_variables += ' -DANDROID_ABI={}'.format(args.arch)
    if args.target == 'linux' and args.arch == 'arm64':
        cmake_extra_variables += ' -DCMAKE_TOOLCHAIN_FILE={}'.format(get_linux_arm64_cmake_toolchain_file(args))
    if not args.default_python_venv:
        cmake_extra_variables += ' -DPF_CUSTOM_PYTHON_VENV_PATH={}'.format(venv_dir)
    if args.crash_reporting:
        cmake_extra_variables += ' -DPF_ENABLE_CRASHREPORTING=1'
    if not args.no_asserts:
        cmake_extra_variables += ' -DPF_ASSERT_ALWAYS=1'
    if args.test_coverage:
        cmake_extra_variables += ' -DPF_ENABLE_COVERAGE=1'
    if args.skip_python_setup:
        cmake_extra_variables += ' -DPF_SKIP_PYTHON_SETUP=1'
    if args.unity:
        cmake_extra_variables += ' -DPF_ENABLE_UNITYBUILD=1'
    # Workaround for WSL Ubuntu 18.04 where python from repo is not supported.
    # Python version is to be 3.9+.
    if host == 'linux':
        cmake_extra_variables += ' -DPF_PYTHON_EXECUTABLE_EXTERNAL=python3'
    if args.D != None:
        for var in args.D:
            cmake_extra_variables += ' -D{}'.format(var.replace('\\', '/'))

    cmake_executable = get_cmake_executable(args)
    cmake_config_cmdline = '{} {}'.format(cmake_executable, make_cmake_generator_option(args))
    cmake_config_cmdline += ' -DPF_TARGET_PLATFORM={}'.format(args.target)
    cmake_config_cmdline += cmake_extra_variables
    cmake_config_cmdline += ' {}'.format(os.path.abspath(project_dir).replace('\\', '/'))
    cmake_config_cmdline += ' -B {}'.format(build_dir)

    if args.fresh:
        cmake_config_cmdline += ' --fresh'
    if args.preset:
        cmake_config_cmdline += ' --preset {}'.format(args.preset)
    
    if args.just_print:
        info('just printing commands')

    info('configuring: {}'.format(cmake_config_cmdline))
    if not args.just_print:
        env = prepare_environment(args)

        # Append cmake configure command line into file
        try:
            f = open(os.path.join(build_dir, 'cmake_config.txt'), 'a')
            print(cmake_config_cmdline, file=f)
            f.close()
        except:
            pass
        
        exit_code=subprocess.run(
                    args=shlex.split(cmake_config_cmdline),
                    cwd=build_dir,
                    env=env).returncode
        if exit_code != 0:
            error('cmake configure failed with code {}'.format(exit_code))
            sys.exit(1)

    if args.build:
        cmake_build_cmdline = '{} --build .'.format(cmake_executable)
        info('building: {}'.format(cmake_build_cmdline))
        if not args.just_print:
            exit_code=subprocess.run(
                    args=shlex.split(cmake_build_cmdline),
                    cwd=build_dir,
                    env=env).returncode
            if exit_code != 0:
                error('cmake build failed with code {}'.format(exit_code))

    if args.open:
        cmake_open_cmdline = '{} --open .'.format(cmake_executable)
        info('opening: {}'.format(cmake_open_cmdline))
        if not args.just_print:
            exit_code=subprocess.run(
                    args=shlex.split(cmake_open_cmdline),
                    cwd=build_dir,
                    env=env).returncode
            if exit_code != 0:
                error('cmake open project failed with code {}'.format(exit_code))

if __name__ == '__main__':
    main()
