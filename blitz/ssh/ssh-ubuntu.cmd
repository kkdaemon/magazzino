@echo off

set CMD=ssh
set IP=

if "%1" == "" goto usage

if "%1" == "01" set IP=192.168.88.37
if "%1" == "02" set IP=192.168.88.32
if "%1" == "04" set IP=192.168.88.43
if "%1" == "05" set IP=192.168.88.44
if "%1" == "06" set IP=192.168.88.71
if "%1" == "07" set IP=192.168.88.72

if "%IP%" == "" goto unknown_agent

if NOT "%2" == "" set CMD=%2
%CMD% -i %USERPROFILE%\.ssh\id_rsa maxim.lazarev@%IP%

goto quit

:unknown_agent
echo Unknown agent number: %1
goto quit

:usage
echo Usage: ssh-ubuntu agent_number [command]
echo   Where command is:
echo     ssh (default)
echo     sftp
goto quit

:quit
