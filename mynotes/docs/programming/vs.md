#### 64-bit toolchain

Set environment variable **PreferredToolArchitecture**  
`set PreferredToolArchitecture=x64`  

<hr/>

#### /Zl option (Omit Default Library Name)

Useful for compiling C static library which is not dependent on runtime type: dynamic or static, debug or release.

<https://docs.microsoft.com/en-us/cpp/build/reference/zl-omit-default-library-name>
