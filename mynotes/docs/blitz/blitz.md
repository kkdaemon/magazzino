### Blitzops project generation

CMake parameters:  
`-DBZ_ASSETS_DEPLOY_RULE=<deploy rule> -DBZ_ASSETS_DEPLOY_PLATFORM=<platform> -DBZ_REMOTE_CONFIG_KEY=<commit hash> -DBZ_BUILD_NAME=<name>`  

Values for `BZ_ASSETS_DEPLOY_RULE`:

    - base
    - production
    - (base)and(production)

values for `BZ_ASSETS_DEPLOY_PLATFORM`:

    - ios_base
    - ios_astc
    - android_base
    - desktop_bc3


`BZ_REMOTE_CONFIG_KEY` takes hash of the commit with tag named like this: `testflight-2.1-30`  
`BZ_BUILD_NAME` is arbitrary name of the build  


How to get commit hash:  
`git tag -l --contains HEAD | grep testflight`, sample output `testflight-1.6-7`  
`git rev-list -n 1 testflight-1.6-7`, sample output `7a1be990b6ad091826ae8440b04fdbdaee2b3856`  


### Blitzops cmake snippets

Desktop:  
`py gen.py blitzops/client -DBZ_ASSETS_DEPLOY_RULE="base" -DBZ_REMOTE_CONFIG_KEY=281b25e228d8a5738a5b39521511c3973bdbeb32 -DBZ_BUILD_NAME=persepolis`  

Mobile (iOS):  
`py gen.py blitzops/client -t ios -DBZ_ASSETS_DEPLOY_RULE="base" -DBZ_ASSETS_DEPLOY_PLATFORM=ios_base -DBZ_REMOTE_CONFIG_KEY=281b25e228d8a5738a5b39521511c3973bdbeb32 -DBZ_BUILD_NAME=persepolis`  

Mobile (Android):  
`py gen.py blitzops/client -t android -DBZ_ASSETS_DEPLOY_RULE="base" -DBZ_ASSETS_DEPLOY_PLATFORM=android_base -DBZ_REMOTE_CONFIG_KEY=281b25e228d8a5738a5b39521511c3973bdbeb32 -DBZ_BUILD_NAME=persepolis`  


### Rules for building DLC URLs

Get DLC config:  
`http://files.cdn-blitzteam.com.s3-website.eu-central-1.amazonaws.com/configs/a54d6db569bed939f4a540d1bf1e9b28a63dd331.yaml`

Field `assets_hash` contains something like `release-2.3/a54d6db569bed939f4a540d1bf1e9b28a63dd331`  

URLs for downloading assets:  
desktops:  
`http://files.cdn-blitzteam.com/release-2.3/a54d6db569bed939f4a540d1bf1e9b28a63dd331/desktop_bc3/`  
ios:  
`http://files.cdn-blitzteam.com/release-2.3/a54d6db569bed939f4a540d1bf1e9b28a63dd331/ios_astc/`  
`http://files.cdn-blitzteam.com/release-2.3/a54d6db569bed939f4a540d1bf1e9b28a63dd331/ios_base/`  
android:  
`http://files.cdn-blitzteam.com/release-2.3/a54d6db569bed939f4a540d1bf1e9b28a63dd331/android/`  


### Docker

View images:  
`docker images`  

    REPOSITORY                        TAG                          IMAGE ID        CREATED         SIZE
    build-unittest                    latest                       d0b0fd881612    2 hours ago     2.96GB
    kkdaemon/cheers2019               latest                       7d4e5ec9c9ad    2 hours ago     4.01MB
    <none>                            <none>                       d767fc57ba9f    2 hours ago     357MB
    golang                            1.11-alpine                  17915ddcabc4    6 days ago      312MB
    gcr.io/aaa-shooter-207210/build   centos7-python37-cmake3_13   ff836e5bf7ce    4 months ago    2.96GB

<hr/>

Start blitz container with shell:  
`docker run --rm -i -t -v D:\projects\blitz\blitz.engine:/tmp/blitz.engine gcr.io/aaa-shooter-207210/build:centos7-python37-cmake3_13 /bin/sh`  

Start container overriding entry point:  
`docker run -it --entrypoint '/bin/bash' <container-name>`  

<hr/>
