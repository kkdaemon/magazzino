# SSH

### Start SSH agent

`eval $(ssh-agent -s)`  

### Add key

`ssh-add ~/.ssh/id_rsa`  

### Fixing "WARNING: UNPROTECTED PRIVATE KEY FILE!"

`chmod 600 ~/.ssh/id_rsa`  
`chmod 600 ~/.ssh/id_rsa.pub`  

<https://www.howtogeek.com/168119/fixing-warning-unprotected-private-key-file-on-linux/>

<hr/>

### File transfer: scp

Copy file from local to remote  

```  
scp myfile1.txt [myfile2.txt] remoteuser@remoteserver:/remote/folder/
scp [-r] * remoteuser@remoteserver:/remote/folder/
```

Copy file from remote to local  

```
scp remoteuser@remoteserver:/remote/folder/remotefile.txt localfile.txt
scp remoteuser@remoteserver:/remote/folder/remotefile.txt .
```


### File transfer: sftp

```
sftp remoteuser@remoteserver

sftp> bye | exit | quit
sftp> pwd
sftp> get [-afpR] remote [local]
sftp> put [-afpR] local [remote]
sftp> help
```
