# Commands

### List directories with sizes

`du -sh *` or  
`du -sk *`  

with sorting  
`du -sh * | sort -h` or `du -sk * | sort -n`  

</hr>
