### Android: File Locations

<b>crashlytics.log</b>  
Windows and Linux: `~/.crashlytics/com.crashlytics.tools/crashlytics.log`  
MacOs: `~/Library/Caches/com.crashlytics/com.crashlytics.tools/crashlytics.log`  

<b>Cached crashlytics versions</b>  
`~/.gradle/caches/modules-2/files-2.1/com.crashlytics.sdk.android`  

<hr/>

### Android: Links

<b>Available Crashlytics versions</b>  
<https://maven.fabric.io/public/io/fabric/tools/gradle/maven-metadata.xml>  

<b>Fabric Gradle Plugin Release Notes</b>  
<https://docs.fabric.io/android/changelog.html#fabric-gradle-plugin>  

<hr/>

