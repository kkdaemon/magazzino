## Command line tips

#### Powershell Autorun Script

Create file with commands at `$env:USERPROFILE\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1` 

#### Cmd AutoRun Script

For Windows 10/11 create registry entry `AutoRun` with script path at `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Command Processor`.
For older Windows versions use `HKEY_CURRENT_USER\Software\Microsoft\Command Processor`

#### Aliases in cmd

Use `DOSKEY` command:

```
DOSKEY ls=dir $*
DOSKEY pwd=echo %%cd%%

DOSKEY b=cd /d "path"
```

#### Enable/disable network interface

Show available network interfaces:  
`netsh interface show interface`  

    Admin State    State          Type             Interface Name
    -------------------------------------------------------------------------
    Enabled        Connected      Dedicated        Ethernet
    Enabled        Connected      Dedicated        vEthernet (nat)
    Enabled        Connected      Dedicated        vEthernet (DockerNAT)

Enable or disable interface by name (need administartor rights):  
`netsh interface set interface "Ethernet" admin=enable`  
`netsh interface set interface "Ethernet" admin=disable`  

<hr/>

#### Control panel items

|                                |                                                 |
|--------------------------------|-------------------------------------------------|
| Add or Remove Programs         | control appwiz.cpl                              |
| Administrative Tools           | control /name Microsoft.AdministrativeTools     |
|                                | control admintools                              |
| Devices and Printers           | control /name Microsoft.DevicesAndPrinters      |
|                                | control printers                                |
| Network and Sharing Center     | control /name Microsoft.NetworkAndSharingCenter |
| Network connections            | control netconnections                          |
|                                | control ncpa.cpl                                |
| ODBC Data Source Administrator | control odbccp32.cpl                            |
| Programs and Features          | control /name Microsoft.ProgramsAndFeatures     |
|                                | control appwiz.cpl                              |
| System                         | control /name Microsoft.System                  |
| System Properties              | control sysdm.cpl                               |
| User Accounts                  | control /name Microsoft.UserAccounts            |
|                                | control userpasswords                           |
| Windows Firewall               | control /name Microsoft.WindowsFirewall         |
|                                | control firewall.cpl                            |

<https://www.lifewire.com/command-line-commands-for-control-panel-applets-2626060>

<hr/>
