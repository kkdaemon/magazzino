# Git
### Environment variables

`set GIT_CURL_VERBOSE=1` verbose curl when git is fetching/pulling </br>
`GIT_SSH=<path to ssh executable>` use ssh on windows </br>

<hr/>

### Remotes

`git remote -v` </br>
`git remote add <name> <url>` </br>
`git remote set-url origin <new_url>` </br>
`git remote remove <name>` </br>
`git remote show origin` - show remote detailed info </br>

<https://git-scm.com/docs/git-remote>

<hr/>

### Branching and checkout

`git checkout -b local_branch --track origin/remote_branch` </br>
`git branch --list [pattern]` list local branches </br>
`git branch -r --list` list remote branches </br>
`git branch -a --list` list local and remote branches </br>

- <https://git-scm.com/docs/git-branch>
- <https://git-scm.com/docs/git-checkout>

<hr/>

### Clean and reset

`git reset --hard` </br>
`git reset --soft HEAD^` undo commit </br>
`git clean -f -d` remove untracked files and directories </br>

- <https://git-scm.com/docs/git-reset>
- <https://git-scm.com/docs/git-clean>

<hr/>

### Diff

1. Diff between branches:  
`git diff master..feature`  
2. Diff between branches for selected folder:  
`git diff master..feature path/to/folder`  

- <https://devconnected.com/how-to-compare-two-git-branches/>  

<hr/>

### Patch: create

`git diff > patch.diff` </br>
`git diff HEAD > patch.diff` </br>
`git diff --cached > patch.diff` - only staged changes </br>
`git diff <commit> <commit> > patch.diff` </br>

- <https://git-scm.com/docs/git-diff>

<hr/>

### Patch: apply

`git apply --ignore-whitespace --ignore-space-change patch.diff` </br>

- <https://git-scm.com/docs/git-apply>

<hr/>

### History

`git log --follow <filename>` single file history </br>
`git log -4 --pretty=oneline` view 4 last commits  

- <https://git-scm.com/docs/git-log>

<hr/>

### other useful commands

`git cherry-pick <commit>` </br>
`git commit --no-verify` skip precommit hooks </br>
`git merge --no-commit` </br>

- <https://git-scm.com/docs/git-cherry-pick>
- <https://git-scm.com/docs/git-commit>
- <https://git-scm.com/docs/git-merge>

<hr/>
