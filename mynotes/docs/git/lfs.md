# LFS

### Skip smudge filter

`GIT_LFS_SKIP_SMUDGE=1 git clone <repo-url>`  

`git config --local filter.lfs.smudge "git-lfs smudge --skip -- %f"`  
`git config --local filter.lfs.process "git-lfs filter-process --skip"`  

`git lfs install --skip-smudge`  

### Download large files

`git lfs pull --include="*.exe"` download only `*.exe` files  
`git lfs pull --exclude="ios,mac"` exclude folders named `ios` or `mac`  

- <https://helpmanual.io/man1/git-lfs-pull/>
- <https://helpmanual.io/man1/git-lfs-fetch/>

### List large files

`git lfs ls-files`  

```
91ccd406ff * doc/editor/image/abgeneralsettings.png
a7865742d7 - doc/editor/image/actiontree.png
```

### View pointer of large file

`git cat-file -p :path/to/file`  

### Convert content to pointers

Use python script `rm-lfs.py` with `git lfs ls-files`:  
`git lfs ls-files | python3 rm-lfs.py`  

```python
# rm-lfs.py
import fileinput
import os

for line in fileinput.input():
    line = line.rstrip()
    delim = line.find('-')
    if delim < 0:
        line.find('*')
    if delim > 0:
        filename = line[delim + 2:]
        try:
            os.remove(filename)
        except:
            print(filename)
```
