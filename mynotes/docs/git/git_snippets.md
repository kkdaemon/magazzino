### Branch renaming

1. Rename local branch `old-name` to `new-name`:  
`git branch -m <old-name> <new-name>` or  
`git branch -m <new-name>` for current branch  
2. Delete `old-name` branch on remote:  
`git push <remote> -d <old-name>` or  
`git push <remote> --delete <old-name>`  
3. Push `new-name` branch to remote:  
`git push <remote> <new-name>`  
4. Reset the upstream branch for `new-name` local branch:  
`git push <remote> -u <new-name>` or  
`git push <remote> --set-upstream <new-name>`  

Links:  
- <https://multiplestates.wordpress.com/2015/02/05/rename-a-local-and-remote-branch-in-git/>  
- <https://stackoverflow.com/a/30590238>

<hr/>
