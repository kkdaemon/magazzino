
    sqllocaldb info "MSSQLLocalDB"
    sqllocaldb start "MSSQLLocalDB"
    sqllocaldb stop "MSSQLLocalDB"

Server name for SSMS or ODBC connection string `(LocalDB)\MSSQLLocalDB`.

System database files are stored in `"c:\Users\<user>\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB"`.

[SqlLocalDB Utility](https://docs.microsoft.com/en-us/sql/tools/sqllocaldb-utility?view=sql-server-2017)
