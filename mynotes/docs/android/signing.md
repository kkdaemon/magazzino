### Get signing certificate fingerprints

```
keytool -list -v -keystore <keystore> -alias <alias> -storepass <password> -keypass <password>
```

**Note**: on Windows MD5 certificate fingerprint is not printed.
