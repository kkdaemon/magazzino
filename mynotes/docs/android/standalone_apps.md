### Installing and running android standalone applications

`/data/local/tmp` - directory where you can upload ELF executable and run it as linux console application.

1. create directory for the app, e.g. `/data/local/tmp/myapp_dir` </br>
    `adb shell mkdir -p /data/local/tmp/myapp_dir`
2. upload application executable, e.g. `myapp` </br>
    `adb push myapp /data/local/tmp/myapp_dir`
3. upload necessary files (shared libraries, resources, etc) </br>
    `adb push libc++_shared.so /data/local/tmp/myapp_dir`
4. set executable permissions for </br>
    `adb shell chmod 0777 /data/local/tmp/myapp_dir/myapp`
5. run the program </br>
    `adb shell cd /data/local/tmp/myapp_dir/myapp; LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH ./myapp`
