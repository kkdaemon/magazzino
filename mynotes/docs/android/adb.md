# ADB
### Using logcat from terminal

- clear logcat messages </br>
  `adb logcat -c`
- start application </br>
  `adb shell am start -n com.blitzteam.unittest/com.blitzteam.unittest.MainActivity`
- show logcat messages with filtering </br>
  `adb logcat -s BLITZ:D AndroidRuntime:E ActivityManager:W`

<https://developer.android.com/studio/command-line/logcat>  

### Screenshots

`adb shell screencap -p /sdcard/screen.png`  
`adb pull /sdcard/screen.png`  
`adb shell rm /sdcard/screen.png`  

### Get APK from device

`adb shell pm path <application-id>`  
`adb pull <path-to-apk`  
or  
`adb shell cp <path-to-apk> <path-on-sdcard>`  
`adb pull <path-on-sdcard>`  
