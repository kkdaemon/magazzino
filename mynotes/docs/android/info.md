# Info

### CPU frequency

`cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_max_freq` 

Maybe used to deduce which cores are big and which cores are little.  
Sample output:

```
spesn:/ $ cat /sys/devices/system/cpu/cpu*/cpufreq/cpuinfo_max_freq
1900800
1900800
1900800
1900800
2400000
2400000
2400000
2400000
```


</hr>

