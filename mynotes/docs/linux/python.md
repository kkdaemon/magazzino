#### Install python3.9 on Ubuntu 18.04 LTS

**Step 1: Add the repository and update**

```
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
```

**Step 2: Install the Python 3.9.0 package using apt-get**

```
sudo apt-get install python3.9
sudo apt install python3.9-venv
sudo python3.9 -m pip install pip
```

**Step 3: Add Python 3.6 & Python 3.9 to update-alternatives**

```
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 2
```

**Step 4: Update Python 3 for point to Python 3.9**

```
sudo update-alternatives --config python3
```

**Step 5: Test the version of python**

```
python3 -V
```

Link: <https://www.itsupportwale.com/blog/how-to-upgrade-to-python-3-9-0-on-ubuntu-18-04-lts/>  
More on `update-alternatives`: <https://linuxhint.com/update_alternatives_ubuntu/>

